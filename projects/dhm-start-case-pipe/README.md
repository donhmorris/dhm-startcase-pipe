# dhm-startcase-pipe
Pipe to format text via lodash startCase
## Why this pipe instead of Angular’s titlecase?
This pipe handles more text formats vs titlecase.
For example, _one-for-the-road_ is formatted as _One For The Road_ using startcase pipe. Titlecase formats it as _One-for-the-road_. The same is true for __one__for__the__road.
## Install
lodash is required. Specifically, the startCase function and its dependencies.

In the root of your project
`npm i dhm-startcase-pipe`

Add the DhmStartCasePipeModule module to your module imports:

    import {BrowserModule} from '@angular/platform-browser'; 
    import {AppRoutingModule} from './app-routing.module';  
    import {AppComponent} from './app.component';  
    import {DhmStartCasePipeModule} from 'dhm-startcase-pipe';  
    import {FormsModule} from '@angular/forms';
    
    @NgModule(   
      declarations: 
      [AppComponent],  
      imports:   
      BrowserModule,  
      AppRoutingModule,  
      DhmStartCasePipeModule,  
      FormsModule  
      ],  
      providers: [],  
      bootstrap: [AppComponent]  
    } )  
    export class AppModule   
    }

## Usage

    <h2>{{someTextVariable | startcase}}</h2>


> Written with [StackEdit](https://stackedit.io/).
