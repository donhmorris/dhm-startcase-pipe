import {Pipe, PipeTransform} from '@angular/core';

declare const _: any;

@Pipe( {
  name: 'startcase'
} )
export class DhmStartCasePipe implements PipeTransform {

  transform( value: unknown, ...args: unknown[] ): unknown {
    return _.startCase( value as string );
  }

}
