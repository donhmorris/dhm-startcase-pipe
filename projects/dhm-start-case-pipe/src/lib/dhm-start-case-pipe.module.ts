import {NgModule} from '@angular/core';
import {DhmStartCasePipe} from './dhm-start-case-pipe';


@NgModule( {
  declarations: [
    DhmStartCasePipe
  ],
  imports: [],
  exports: [
    DhmStartCasePipe
  ]
} )
export class DhmStartCasePipeModule {
}
